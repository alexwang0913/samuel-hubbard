const templates = [
	{
		id: 'wishlist-link',
		data: 'wishlist',
		template: `
      <a href="{{ wishlist.url }}" class="wk-link wk-link--{{ wishlist.state }}" title="{{ locale.view_wishlist }}">
        <div class="wk-icon wk-link__icon">{% include "wishlist-icon" %}</div>
        <span class="wk-link__label">{{ locale.wishlist }}</span>
        <span class="wk-link__count">{{ wishlist.item_count }}</span>
      </a>
    `,
	},
	{
		id: 'wishlist-button',
		data: 'product',
		events: {
			'click button[data-wk-add-product]': (event) => {
				event.preventDefault();
				event.stopPropagation();

				const select = document.querySelector("form select[name='id']");
				WishlistKing.toolkit.addProduct(
					event.currentTarget.getAttribute('data-wk-add-product'),
					select ? select.value : undefined
				);

				let product = event.currentTarget.closest('.product-card');

				wishlistStatus();

				// klaviyo tracking
				_learnq.push([
					'track',
					'Added to Wishlist',
					{
						ProductName: product.querySelector('.h6').textContent,
						ProductID: product.getAttribute('data-product-id'),
						ImageURL: product.querySelector('.pc-image-og').querySelector('img')
							.src,
						URL: product.querySelector('.full-unstyled-link').href,
						Price: parseInt(
							product.querySelector('.pc-details-price').textContent
						),
						Quantity: 1,
					},
				]);
			},
			'click button[data-wk-remove-product]': (event) => {
				event.preventDefault();
				event.stopPropagation();

				wishlistStatus();

				WishlistKing.toolkit.removeProduct(
					event.currentTarget.getAttribute('data-wk-remove-product')
				);
			},
			'click button[data-wk-remove-item]': (event) => {
				event.preventDefault();
				event.stopPropagation();

				WishlistKing.toolkit.removeItem(
					event.currentTarget.getAttribute('data-wk-remove-item')
				);
			},
		},
		template: `
      {% if product.in_wishlist %}
        {% assign btn_text = locale.in_wishlist %}
        {% assign btn_title = locale.remove_from_wishlist %}
        {% assign btn_action = 'remove' %}
      {% else %}
        {% assign btn_text = locale.add_to_wishlist %}
        {% assign btn_title = locale.add_to_wishlist %}
        {% assign btn_action = 'add' %}
      {% endif %}

      {% assign scope = "product" %}
      {% assign targetId = product.id %}
      {% assign icon_name = "wishlist-icon" %}

      {% if itemId %}
      {% assign scope = "item" %}
      {% assign targetId = itemId %}
      {% assign icon_name = "remove-icon" %}
      {% endif %}

      <button type="button" class="wk-button wk-button--{{ btn_action }} {{ addClass }}" title="{{ btn_title }}" data-wk-{{ btn_action }}-{{ scope }}="{{ targetId }}">
        <div class="wk-icon wk-button__icon">{% include icon_name %}</div>
        <span class="wk-button__label">{{ btn_text }}</span>
      </button>
    `,
	},
	{
		id: 'wishlist-button-floating',
		data: 'product',
		template: `
      {% include "wishlist-button" addClass: "wk-button--floating" %}
    `,
	},
	{
		id: 'wishlist-page',
		data: 'wishlist',
		events: {
			'click a[data-wk-share]': (event) => {
				event.preventDefault();
				event.stopPropagation();

				WishlistKing.toolkit.requestWishlistSharing({
					wkShareService: event.currentTarget.getAttribute(
						'data-wk-share-service'
					),
					wkShare: event.currentTarget.getAttribute('data-wk-share'),
					wkShareImage: event.currentTarget.getAttribute('data-wk-share-image'),
				});
			},
		},
		template: `
      <div class='wk-page'>
      	{% if wishlist.item_count == 0 %}
					<div class="wk-note wk-note__list-empty c-3 type-spacing">
						<p>{{ locale.wishlist_empty_note }}</p>
						<a class="button button--primary color-background-1" href="/">Start Shopping</a>
					</div>
    	  {% else %}
					{% unless wishlist.read_only %}
						<div class="wk-sharing">
							<p>Share your wishlist to drop a hint. It's a great way to get whats on your list!</p>
							<ul class="wk-sharing__list">
								{% comment %}
									<li class="wk-sharing__list-item">{% include "wishlist-share-button-fb" %}</li>
									<li class="wk-sharing__list-item">{% include "wishlist-share-button-twitter" %}</li>
									<li class="wk-sharing__list-item">{% include "wishlist-share-button-whatsapp" %}</li>
									<li class="wk-sharing__list-item">{% include "wishlist-share-button-contact" %}</li>
								{% endcomment %}
								<li class="wk-sharing__list-item">{% include "wishlist-share-button-email" %}</li>
								<li class="wk-sharing__list-item">{% include "wishlist-share-button-link" %}</li>
							</ul>
							<div class="wk-sharing__link wk-sharing__link--hidden"><span class="wk-sharing__link-text"></span><button class="wk-sharing__link__copy-button" data-clipboard-target=".wk-sharing__link-text">{{ locale.copy_share_link }}</button></div>
						</div>
					{% endunless %}

					{% if customer_accounts_enabled and customer == null and wishlist.read_only == false %}
						<div class="wk-note wk-note__login c-3">
							<p>{{ locale.login_or_signup_note }}</p>
						</div>
					{% endif %}

					<div class="wk-grid">
						{% assign item_count = 0 %}
						{% assign products = wishlist.products | reverse %}
						{% for product in products %}
							{% assign item_count = item_count | plus: 1 %}
							{% unless limit and item_count > limit %}
								{% assign hide_default_title = false %}
								{% if product.variants.length == 1 and product.variants[0].title contains 'Default' %}
									{% assign hide_default_title = true %}
								{% endif %}

								{% assign variant = product.selected_or_first_available_variant %}
								{% if variant.price < variant.compare_at_price %}
									{% assign onsale = true %}
								{% else %}
									{% assign onsale = false %}
								{% endif %}
								
								{% capture product_json %}
									[
										{% for v in product.variants %}
											{
												'title': '{{ v.title }}',
												'availability': {{ v.available }},
												'id': {{ v.id }}
											}
											{% unless forloop.last %},{% endunless forloop.last %}
										{% endfor %}
									]
								{% endcapture %}

								<div 
									class="product-card wk-grid__item {% if onsale %}wk-product--sale{% endif %}" data-wk-item="{{ product.wishlist_item_id }}"
									data-product-variants="{{ product_json | strip_newlines | replace: '	', '' }}">
									{% unless wishlist.read_only %}
										{% include "wishlist-button-floating" itemId: product.wishlist_item_id %}
									{% else %}
										{% include "wishlist-button-floating" product: product %}
									{% endunless %}

									<div class='pc-image'>
										<a href="{{ product | variant_url }}" class="full-unstyled-link object-contain bg-subtle">
											<img src="{{ product | variant_img_url: '1000x' }}">
										</a>

										<div class="pc-quick-add">
											<div class="pc-quick-add-label p-fill t-b-2-m">+ Quick Add</div>
											<div class="pc-quick-add-selectors f-h cr-black">
												{% for option in product.options_with_values %}
													{% if option.name != 'Color' %}
														<div class="selector" data-index="{{ forloop.index }}">
															<div class="selector-label t-b-2-m">
																<span class="selector-label-name">{{ option.name }}</span>
																<img src="//cdn.shopify.com/s/files/1/0542/1681/8875/t/12/assets/svg-caret-down-black.svg?v=18285259987802779865" alt="">
															</div>

															<ul class="f-h t-b-2 cr-gray-1">
																{% for value in option.values %}
																	<li>
																		<button 
																			class="selector-value"
																			data-value="{{ value }}">
																		{{ value }}</button>
																	</li>
																{% endfor %}
															</ul>
														</div>
													{% endif %}
												{% endfor %}
											</div>

											<button 
												class="pc-quick-add-trigger button--primary color-background-1"
												variant>
											Add to Cart</button>
										</div>
									</div>

									<div class='pc-details'>
										<a href="{{ product | variant_url }}" class="full-unstyled-link">
											<h6 class='h6'>{{ product.title }}</h6>
											<div class='pc-flex-row'>
												{% if product.metafields.custom_fields.product_subtitle %}
													<span class='pc-details-subtitle body-small text-subdued'>{{ product.metafields.custom_fields.product_subtitle }}</span>
												{% endif %}
												<span class='pc-details-price body-tall'>{{ variant.price | money }}</span>
											</div>
										</a>
									</div>
								</div>
							{% endunless %}
						{% endfor %}
					</div>

					{% comment %}
						{% unless wishlist.read_only %}
							{% include "wishlist-button-clear" %}
						{% endunless %}
					{% endcomment %}

					{% comment %}
						{% include "wishlist-button-bulk-add-to-cart" %}
					{% endcomment %}
				{% endif %}
      </div>
    `,
	},
	{
		id: 'wishlist-product-form',
		events: {
			'render .wk-product-form': (form) => {
				const container = form.closest('[data-wk-item]');
				const itemId = container.getAttribute('data-wk-item');
				WishlistKing.toolkit.getItem(itemId).then((product) => {
					WishlistKing.toolkit.initProductForm(form, product, {
						// NOTE: Uncomment to override default option change
						// onOptionChange: (event) => {
						//   console.log(event.dataset);
						// },
						// NOTE: Uncomment to override default form submit
						// onFormSubmit: (event) => {
						//   event.preventDefault();
						//   event.stopPropagation();
						// },
					});
				});
			},
		},
		template: `
      <form class="wk-product-form" action="/cart/add" method="post">
        {% assign current_variant = product.selected_or_first_available_variant %}
        <div class="wk-product-form__options">
          <input name="id" value="{{ current_variant.id }}" type="hidden">
          {% unless product.has_only_default_variant %}
            {% for option in product.options_with_values %}
              <div class="wk-product-form__option">
                <label class="wk-product-form__option__label" for="Option{{ option.position }}">
                  {{ option.name }}
                </label>
                <select class="wk-product-form__option__select" name="options[{{ option.name | escape }}]">
                  {% for value in option.values %}
                    <option value="{{ value | escape }}" {% if option.selected_value == value %}selected="selected"{% endif %} {% if option.soldout_values contains value %}disabled{% endif %}>
                      {{ value }}
                    </option>
                  {% endfor %}
                </select>
              </div>
            {% endfor %}
          {% endunless %}
          {% comment %}
          <div class="wk-product-form__quantity">
            <label class="wk-product-form__quantity__label" for="Quantity">{{ locale.quantity }}</label>
            <input class="wk-product-form__quantity__input" type="number" name="quantity" value="1" min="1">
          </div>
          {% endcomment %}
        </div>
        <button type="submit" class="wk-product-form__submit" data-wk-add-to-cart="{{ product.wishlist_item_id }}" {% unless current_variant.available %}disabled{% endunless %}>
          {% if current_variant.available %}{{ locale.add_to_cart }}{% else %}{{ locale.sold_out }}{% endif %}
        </button>
      </form>
    `,
	},
	{
		id: 'wishlist-page-shared',
		data: 'shared_wishlist',
		template: `
      {% assign wishlist = shared_wishlist %}
      {% include "wishlist-page" with wishlist %}
    `,
	},
	{
		id: 'wishlist-button-bulk-add-to-cart',
		data: 'wishlist',
		events: {
			'click button[data-wk-bulk-add-to-cart]': (event) => {
				WishlistKing.toolkit.requestAddAllToCart(
					event.currentTarget.getAttribute('data-wk-bulk-add-to-cart')
				);
			},
		},
		template: `
      {% assign btn_text = locale.add_all_to_cart %}
      {% assign btn_title = locale.add_all_to_cart %}

      <button type="button" class="wk-button-bulk-add-to-cart" title="{{ btn_title }}" data-wk-bulk-add-to-cart="{{ wishlist.permaId }}">
        <span class="wk-label">{{ btn_text }}</span>
      </button>
    `,
	},
	{
		id: 'wishlist-button-clear',
		data: 'wishlist',
		events: {
			'click button[data-wk-clear-wishlist]': (event) => {
				WishlistKing.toolkit.clear(
					event.currentTarget.getAttribute('data-wk-clear-wishlist')
				);
			},
		},
		template: `
      {% assign btn_text = locale.clear_wishlist %}
      {% assign btn_title = locale.clear_wishlist %}

      <button type="button" class="wk-button-wishlist-clear" title="{{ btn_title }}" data-wk-clear-wishlist="{{ wishlist.permaId }}">
        <span class="wk-label">{{ btn_text }}</span>
      </button>
    `,
	},
	{
		id: 'wishlist-icon',
		template: `
      <svg class="wk-icon__svg" width="100%" height="100%" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <path vector-effect="non-scaling-stroke" d="M32.012,59.616c-1.119-.521-2.365-1.141-3.707-1.859a79.264,79.264,0,0,1-11.694-7.614C6.316,42,.266,32.6.254,22.076,0.244,12.358,7.871,4.506,17.232,4.5a16.661,16.661,0,0,1,11.891,4.99l2.837,2.889,2.827-2.9a16.639,16.639,0,0,1,11.874-5.02h0c9.368-.01,17.008,7.815,17.021,17.539,0.015,10.533-6.022,19.96-16.312,28.128a79.314,79.314,0,0,1-11.661,7.63C34.369,58.472,33.127,59.094,32.012,59.616Z"/>
      </svg>
    `,
	},
	{
		id: 'remove-icon',
		template: `
      <svg class="wk-icon__svg" width="100%" height="100%" viewBox="0 0 64 64" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <path vector-effect="non-scaling-stroke" d="M0.309,0.309a0.9,0.9,0,0,1,1.268,0L63.691,62.423a0.9,0.9,0,0,1-1.268,1.268L0.309,1.577A0.9,0.9,0,0,1,.309.309Z"/>
        <path vector-effect="non-scaling-stroke" d="M63.691,0.309a0.9,0.9,0,0,1,0,1.268L1.577,63.691A0.9,0.9,0,0,1,.309,62.423L62.423,0.309A0.9,0.9,0,0,1,63.691.309Z"/>
      </svg>
    `,
	},
	{
		id: 'wishlist-share-button-fb',
		data: 'wishlist',
		template: `
      <a href="#" class="wk-share-button" title="{{ locale.share_on_facebook }}" data-wk-share-service="facebook" data-wk-share="{{ wishlist.permaId }}">
        <svg version="1.1" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 24 24">
          <path fill="currentColor" d="M18.768,7.465H14.5V5.56c0-0.896,0.594-1.105,1.012-1.105s2.988,0,2.988,0V0.513L14.171,0.5C10.244,0.5,9.5,3.438,9.5,5.32 v2.145h-3v4h3c0,5.212,0,12,0,12h5c0,0,0-6.85,0-12h3.851L18.768,7.465z"/>
        </svg>
      </a>
    `,
	},
	{
		id: 'wishlist-share-button-twitter',
		data: 'wishlist',
		template: `
      <a href="#" class="wk-share-button" title="{{ locale.share_on_twitter }}" data-wk-share-service="twitter" data-wk-share="{{ wishlist.permaId }}">
        <svg version="1.1" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 24 24">
        <path fill="currentColor" d="M23.444,4.834c-0.814,0.363-1.5,0.375-2.228,0.016c0.938-0.562,0.981-0.957,1.32-2.019c-0.878,0.521-1.851,0.9-2.886,1.104 C18.823,3.053,17.642,2.5,16.335,2.5c-2.51,0-4.544,2.036-4.544,4.544c0,0.356,0.04,0.703,0.117,1.036 C8.132,7.891,4.783,6.082,2.542,3.332C2.151,4.003,1.927,4.784,1.927,5.617c0,1.577,0.803,2.967,2.021,3.782 C3.203,9.375,2.503,9.171,1.891,8.831C1.89,8.85,1.89,8.868,1.89,8.888c0,2.202,1.566,4.038,3.646,4.456 c-0.666,0.181-1.368,0.209-2.053,0.079c0.579,1.804,2.257,3.118,4.245,3.155C5.783,18.102,3.372,18.737,1,18.459 C3.012,19.748,5.399,20.5,7.966,20.5c8.358,0,12.928-6.924,12.928-12.929c0-0.198-0.003-0.393-0.012-0.588 C21.769,6.343,22.835,5.746,23.444,4.834z"/>
        </svg>
      </a>
    `,
	},
	{
		id: 'wishlist-share-button-whatsapp',
		data: 'wishlist',
		template: `
      <a href="#" class="wk-share-button" title="{{ locale.share_with_whatsapp }}" data-wk-share-service="whatsapp" data-wk-share="{{ wishlist.permaId }}">
        <svg xmlns="https://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24">
          <path fill="currentColor" stroke="none" d="M20.1,3.9C17.9,1.7,15,0.5,12,0.5C5.8,0.5,0.7,5.6,0.7,11.9c0,2,0.5,3.9,1.5,5.6l-1.6,5.9l6-1.6c1.6,0.9,3.5,1.3,5.4,1.3l0,0l0,0c6.3,0,11.4-5.1,11.4-11.4C23.3,8.9,22.2,6,20.1,3.9z M12,21.4L12,21.4c-1.7,0-3.3-0.5-4.8-1.3l-0.4-0.2l-3.5,1l1-3.4L4,17c-1-1.5-1.4-3.2-1.4-5.1c0-5.2,4.2-9.4,9.4-9.4c2.5,0,4.9,1,6.7,2.8c1.8,1.8,2.8,4.2,2.8,6.7C21.4,17.2,17.2,21.4,12,21.4z M17.1,14.3c-0.3-0.1-1.7-0.9-1.9-1c-0.3-0.1-0.5-0.1-0.7,0.1c-0.2,0.3-0.8,1-0.9,1.1c-0.2,0.2-0.3,0.2-0.6,0.1c-0.3-0.1-1.2-0.5-2.3-1.4c-0.9-0.8-1.4-1.7-1.6-2c-0.2-0.3,0-0.5,0.1-0.6s0.3-0.3,0.4-0.5c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0-0.4,0-0.5c0-0.1-0.7-1.5-1-2.1C8.9,6.6,8.6,6.7,8.5,6.7c-0.2,0-0.4,0-0.6,0S7.5,6.8,7.2,7c-0.3,0.3-1,1-1,2.4s1,2.8,1.1,3c0.1,0.2,2,3.1,4.9,4.3c0.7,0.3,1.2,0.5,1.6,0.6c0.7,0.2,1.3,0.2,1.8,0.1c0.6-0.1,1.7-0.7,1.9-1.3c0.2-0.7,0.2-1.2,0.2-1.3C17.6,14.5,17.4,14.4,17.1,14.3z"/>
        </svg>
      </a>
    `,
	},
	{
		id: 'wishlist-share-button-email',
		data: 'wishlist',
		template: `
			<a href="#" class="wk-share-button" title="{{ locale.share_by_email }}" data-wk-share-service="email" data-wk-share="{{ wishlist.permaId }}">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 15.56">
					<path fill="currentColor" fill-rule="evenodd"
						d="M16.91,1.5H3.09L8.73,6.33a1.92,1.92,0,0,0,2.54,0ZM1.5,2.11V13.56a.5.5,0,0,0,.5.5H18a.5.5,0,0,0,.5-.5V2.11h0L12.25,7.47a3.43,3.43,0,0,1-4.5,0L1.51,2.12ZM0,2A2,2,0,0,1,2,0H18a2,2,0,0,1,2,2V13.56a2,2,0,0,1-2,2H2a2,2,0,0,1-2-2Z" />
				</svg>
      </a>
    `,
	},
	{
		id: 'wishlist-share-button-link',
		data: 'wishlist',
		template: `
      <a href="#" class="wk-share-button" title="{{ locale.get_link }}" data-wk-share-service="link" data-wk-share="{{ wishlist.permaId }}">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 11">
					<path fill="currentColor"
						d="M6.35,5.5a.92.92,0,0,0,.26.65.91.91,0,0,0,.64.27H12.7a.91.91,0,0,0,.64-.27.94.94,0,0,0,0-1.3.91.91,0,0,0-.64-.27H7.25a.91.91,0,0,0-.64.27A.92.92,0,0,0,6.35,5.5Zm.91,3.67H5.63A3.76,3.76,0,0,1,1.82,5.84,3.69,3.69,0,0,1,2,4.33,3.62,3.62,0,0,1,2.75,3,3.69,3.69,0,0,1,4,2.14a3.76,3.76,0,0,1,1.47-.31H7.26a.9.9,0,0,0,.64-.26A1,1,0,0,0,8.17.92.93.93,0,0,0,7.9.27.91.91,0,0,0,7.26,0H5.63A5.55,5.55,0,0,0,2,1.33,5.61,5.61,0,0,0,.05,4.78,5.64,5.64,0,0,0,.23,7.09a5.54,5.54,0,0,0,1.12,2A5.41,5.41,0,0,0,5.44,11H7.26A.91.91,0,0,0,7.9,9.44.87.87,0,0,0,7.26,9.17ZM20,4.8A5.68,5.68,0,0,0,18,1.32,5.56,5.56,0,0,0,14.3,0H12.93c-.74,0-1.14.41-1.14.92a1,1,0,0,0,.27.65.9.9,0,0,0,.64.26h1.62a3.76,3.76,0,0,1,3.82,3.33A3.69,3.69,0,0,1,18,6.67,3.75,3.75,0,0,1,17.21,8,3.85,3.85,0,0,1,16,8.86a3.69,3.69,0,0,1-1.47.31H12.7a.87.87,0,0,0-.64.27A.92.92,0,0,0,12.7,11h1.81a5.38,5.38,0,0,0,4.12-1.85A5.55,5.55,0,0,0,20,4.8Z" />
				</svg>
      </a>
    `,
	},
	{
		id: 'wishlist-share-button-contact',
		data: 'wishlist',
		template: `
      <a href="#" class="wk-share-button" title="{{ locale.send_to_customer_service }}" data-wk-share-service="contact" data-wk-share="{{ wishlist.permaId }}">
        <svg width="100%" height="100%" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path fill="currentColor" d="M19 2H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h4l3 3 3-3h4c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-6 16h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 11.9 13 12.5 13 14h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/>
        </svg>
      </a>
    `,
	},
];

export default templates;
