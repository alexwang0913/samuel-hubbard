const initPageAnimation = () => {
	document.querySelectorAll('[data-animate]').forEach((item) => {
		let observer = new IntersectionObserver(
			(el) => {
				if (
					el[0].isIntersecting === true &&
					!el[0].target.classList.contains('is-animated')
				) {
					el[0].target.classList.add('is-animated');
				}
			},
			{
				root: null,
				threshold: [0.5], // 0 = page start; 1 = page bottom, items only revealed when in full view
			}
		);

		observer.observe(item);
	});
};

const initAccordion = () => {
	const accordion = document.querySelectorAll('.comp-accordion');

	accordion.forEach((el) => {
		el.querySelector('.accordion-label').addEventListener('click', (e) => {
			let content = el.querySelector('.accordion-content');

			// activate current accordion
			slide('toggle', content);

			if (!el.classList.contains('is-active')) {
				el.classList.add('is-active');
				el.setAttribute('aria-expanded', true);
				content.setAttribute('aria-hidden', false);
			} else {
				el.classList.remove('is-active');
				el.setAttribute('aria-expanded', false);
				content.setAttribute('aria-hidden', true);
			}

			accordion.forEach((el2) => {
				if (el2 != el) {
					// inactive all other accordions
					slide('up', el2.querySelector('.accordion-content'));
					el2.classList.remove('is-active');
					el2
						.querySelector('accordion-label')
						.setAttribute('aria-expanded', false);
					el2.setAttribute('aria-hidden', true);
				}
			});
		});
	});
};

const initSearchPane = () => {
	const searchPane = document.querySelector('.search-pane');
	const searchPaneInput = searchPane.querySelector('.search-pane-input');
	const searchPaneTabs = searchPane.querySelector('.search-pane-tabs');
	const tabProducts = document.querySelector('[search-tab-trigger="products"]');
	const tabArticles = document.querySelector('[search-tab-trigger="articles"]');
	const tabCollections = document.querySelector(
		'[search-tab-trigger="collections"]'
	);
	const searchPaneResults = searchPane.querySelector('.search-pane-results');
	const searchUrl = `${siteUrl}/search`;
	const resultsActionUrl = searchPane.querySelector('.results-actions-url');

	// hide everything until all fetch is done running
	doneFetchProducts = doneFetchArticles = doneFetchCollections = searchTabTrigger = false;

	const searchReset = () => {
		searchPane.querySelector('.results-collections')
			? searchPane.querySelector('.results-collections').remove()
			: '';

		searchPane.querySelector('.results-products')
			? searchPane.querySelector('.results-products').remove()
			: '';

		searchPane.querySelector('.results-articles')
			? searchPane.querySelector('.results-articles').remove()
			: '';

		searchPaneTabs.style.visibility = 'hidden';
		resultsActionUrl.href = '';
		root.classList.remove('search-pane-has-value');
	};

	const searchTabFindActive = () => {
		let tabTriggers = searchPaneTabs.querySelectorAll(
			'[search-tab-trigger].has-results'
		);

		clearTimeout(searchTabTrigger);

		if (
			doneFetchProducts &&
			doneFetchArticles &&
			doneFetchCollections &&
			tabTriggers
		) {
			searchTabTrigger = setTimeout(() => {
				searchPaneTabs.style.visibility = 'visible';
				tabTriggers[0].click();
			}, 100);
		}
	};

	searchInputThrottle = null;

	const searchRun = () => {
		clearTimeout(searchInputThrottle);
		searchReset();

		searchInputThrottle = setTimeout(function() {
			let term = searchPaneInput.value;
			tabProducts.innerHTML = '';
			tabArticles.innerHTML = '';
			tabCollections.innerHTML = '';
			tabProducts.classList.remove('has-results');
			tabArticles.classList.remove('has-results');
			tabCollections.classList.remove('has-results');

			if (term.length == 0) {
				return false;
			}

			if (term.length < 3 || term == searchPane.dataset.searchTerm) {
				return false;
			}

			// add active state
			root.classList.add('search-pane-has-value');

			// register search term to avoid double triggering ajax
			searchPane.setAttribute('data-search-term', term);

			// PRODUCTS
			fetch(`${searchUrl}?type=product&q=${term}&view=json`)
				.then(function(response) {
					return response.json();
				})
				.then((data) => {
					searchPaneResults.querySelector('.results-products')
						? searchPaneResults.querySelector('.results-products').remove()
						: '';

					doneFetchProducts = true;

					// exit function if no results found
					if (data.results_count == 0) return false;

					let insertDiv = document.createElement('div');
					insertDiv.classList.add('results-products', 'f-h');
					insertDiv.setAttribute('search-tab', 'products');

					tabProducts.classList.add('has-results');
					tabProducts.innerHTML = `Products (${data.results_count})`;

					searchTabFindActive();

					searchProductsHtml = '';
					data.results.forEach((item, i) => {
						if (i >= 4) return false;
						searchProductsHtml += `
							<li class="search-product">
								<div class="search-product-thumbnail">
									${
										!item.thumbnail.includes('no-image')
											? `<span class="object-contain bg-subtle">
													<img 
														src="${item.thumbnail}" 
														alt="${item.title}">
													</span>`
											: ''
									}
								</div>

								<div class="search-product-info cr-black">
									<div class="t-h-6">${item.title}</div>
									<div class="f-h-center">
										${
											item.subtitle
												? `<span class="product-info-subtitle t-b-2 cr-gray-1">${item.subtitle}</span>`
												: ''
										}
										<span class="product-info-price t-b-1">${item.price ? item.price : ''}</span>
									</div>
									<!--<div class="t-l-2 cr-gray-1">+${
										item.variantsCount
									} colors available</div>-->
								</div>

								<a 
									class="p-fill" 
									href="${item.url}" 
									title="${item.title}"></a>
							</li>
						`;
					});
					searchProductsHtml += `<ul>`;
					insertDiv.innerHTML = searchProductsHtml;
					searchPaneResults.prepend(insertDiv);
				});

			// ARTICLES
			fetch(`${searchUrl}?type=article&q=${term}&view=json`)
				.then(function(response) {
					return response.json();
				})
				.then((data) => {
					searchPaneResults.querySelector('.results-articles')
						? searchPaneResults.querySelector('.results-articles').remove()
						: '';

					doneFetchArticles = true;

					// exit function if no results found
					if (data.results_count == 0) return false;

					let insertDiv = document.createElement('div');
					insertDiv.classList.add('results-articles');
					insertDiv.setAttribute('search-tab', 'articles');

					tabArticles.classList.add('has-results');
					tabArticles.innerHTML = `Journal (${data.results_count})`;

					searchTabFindActive();

					searchArticlesHtml = '<ul>';
					data.results.forEach((item, i) => {
						if (i >= 3) return false;

						searchArticlesHtml += `
							<li class="article-card-compact">
							<div class="f-h">
								<div class="card-thumbnail bg-subdued">
									<span class="object-fit">
										<img src="${item.thumbnail}">
									</span>
								</div>

								<div class="card-info f-v">
									<div class="card-info-title t-h-3">${item.title}</div>
									<div class="card-info-excerpt t-b-1 cr-gray-2">
										<!--${item.excerpt}-->
									</div>

									<div class="card-info-footer t-c-1">
										<a class="button button--secondary" href="${item.url}">Read More</a>
									</div>
								</div>

								<a 
									class="p-fill" 
									href="${item.url}" 
									title="Read the article - {{ article.title | escape }}"> </a>
							</div>
							</li>
						`;
					});
					searchArticlesHtml += `<ul>`;
					insertDiv.innerHTML = searchArticlesHtml;
					searchPaneResults.prepend(insertDiv);
				});

			// COLLECTIONS
			fetch(`${searchUrl}?type=colection&q=${term}&view=json`)
				.then(function(response) {
					return response.json();
				})
				.then((data) => {
					searchPaneResults.querySelector('.results-collections')
						? searchPaneResults.querySelector('.results-collections').remove()
						: '';

					doneFetchCollections = true;

					// exit function if no results found
					if (data.results_count == 0) return false;

					let insertDiv = document.createElement('div');
					insertDiv.classList.add('results-collections');
					insertDiv.setAttribute('search-tab', 'collections');

					tabCollections.classList.add('has-results');
					tabCollections.innerHTML = `Collections (${data.results_count})`;

					searchTabFindActive();

					searchCollectionsHtml = '<ul>';
					data.results.forEach((item, i) => {
						if (i >= 6) return false;

						searchCollectionsHtml += `
							<li>
								<a href="${item.url}">${item.title}</a>
							</li>
						`;
					});
					searchCollectionsHtml += `<ul>`;
					insertDiv.innerHTML = searchCollectionsHtml;
					searchPaneResults.prepend(insertDiv);
				});
		}, 50);

		resultsActionUrl.href = `${searchUrl}?q=${searchPaneInput.value}`;
	};

	window.addEventListener('click', (e) => {
		if (
			e.target.classList.contains('search-open') ||
			e.target.closest('.search-open')
		) {
			root.classList.add('search-pane-is-active');

			setTimeout(() => {
				searchPaneInput.focus();
			}, 400);
		}

		if (
			e.target.hasAttribute('search-tab-trigger') ||
			e.target.closest('[search-tab-trigger]')
		) {
			let target = e.target.hasAttribute('search-tab-trigger')
				? e.target
				: e.target.closest('[search-tab-trigger]');

			document.querySelectorAll('[search-tab-trigger]').forEach((el) => {
				el.classList.remove('is-active');
			});

			document.querySelectorAll('[search-tab]').forEach((el) => {
				el.classList.remove('is-active');
			});

			setTimeout(() => {
				target.classList.add('is-active');
				document
					.querySelector(
						`[search-tab="${target.getAttribute('search-tab-trigger')}"]`
					)
					?.classList.add('is-active');
			}, 1);
		}

		if (
			(!e.target.classList.contains('search-pane') &&
				!e.target.closest('.search-pane') &&
				!e.target.classList.contains('.search-open') &&
				!e.target.closest('.search-open')) ||
			e.target.closest('.search-pane-close') ||
			e.target.classList.contains('search-pane-close')
		) {
			root.classList.remove('search-pane-is-active');
			setTimeout(searchReset, 400);
		}
	});

	searchPaneInput.addEventListener('keypress', (e) => {
		let keycode = String.fromCharCode(e.keyCode);

		if (e.keyCode == 13 && root.classList.contains('search-pane-has-value')) {
			location.href = document.querySelector('.results-actions-url').href;
		} else if (/[a-zA-Z0-9-_ ]/.test(keycode)) {
			searchRun();
		}
	});
};

const initSearchIndex = () => {
	const resultsTabTrigger = document.querySelectorAll(
		'[data-results-tab-trigger]'
	);
	const resultsTab = document.querySelectorAll('[data-results-tab]');

	resultsTabTrigger.forEach((item) => {
		item.addEventListener('click', () => {
			resultsTabTrigger.forEach((el) => {
				el.dataset.resultsTabTrigger == item.dataset.resultsTabTrigger
					? el.classList.add('is-active')
					: el.classList.remove('is-active');
			});

			resultsTab.forEach((el) => {
				el.dataset.resultsTab == item.dataset.resultsTabTrigger
					? el.classList.add('is-active')
					: el.classList.remove('is-active');
			});
		});
	});
};

const initBlogIndex = () => {
	if (document.querySelector('blog-pagination-next')) {
		let infiniteScroll = new InfiniteScroll('.blog-load-more', {
			path: '.blog-pagination-next',
			append: '.blog-post',
			button: '.blog-load-more-trigger',
			scrollThreshold: false,
			history: false,
		});

		infiniteScroll.on('last', function(body, path) {
			let loadMoreTrigger = document.querySelector('.blog-load-more-trigger');

			loadMoreTrigger.style.display = 'block';
			loadMoreTrigger.disabled = true;
			loadMoreTrigger.innerHTML = 'You’ve reached the end';
		});
	}
};

const initProductSingle = () => {
	// colorway selection
	on('body', 'click', '.js-product-colorway-open', (e, el) => {
		e.preventDefault();
		let target = e.target.classList.contains('js-product-colorway-open')
			? e.target
			: e.target.closest('.js-product-colorway-open');
		let selectedSwatch = document?.querySelector(
			`.each_colorway[href*="${target.getAttribute('href')}"]`
		);

		// swap color way selector
		document.querySelectorAll('.each_colorway').forEach((el) => {
			el.classList.remove('current_selection');
		});

		selectedSwatch.classList.add('current_selection');

		if (document.querySelector('.colorway_label')) {
			document.querySelector('.colorway_label').textContent =
				selectedSwatch.dataset.name;
			document
				.querySelector('.colorway_label')
				.setAttribute('data-name', selectedSwatch.dataset.name);
		}

		document.querySelector('.product-option-color-current').value =
			selectedSwatch.dataset.name;

		// swap sticky add to cart color way
		let stickyAddToCart = document.querySelector('.sticky-add-to-cart');

		if (stickyAddToCart) {
			stickyAddToCartDetails = stickyAddToCart.querySelector('.satc_details');

			stickyAddToCart
				.querySelectorAll('.js-product-colorway-open')
				.forEach((el) => {
					if (el.href == target.href) {
						stickyAddToCart
							.querySelector('img')
							.setAttribute('src', el.querySelector('img').getAttribute('src'));

						stickyAddToCart.querySelector('img').removeAttribute('srcset');

						stickyAddToCart
							.querySelector('.satc_Color')
							.querySelector('span').textContent = el.querySelector(
							'.body-small'
						).textContent;
					}
				});
		}

		// update form info
		document.querySelector('.last-chance').innerHTML = '';

		let selectedWidth =
			document.querySelector('.option-width')?.querySelector('input:checked') ||
			false;

		// fetch and replace content
		root.classList.add('pdp-is-transitioning');

		fetch(target.href)
			.then(function(response) {
				return response.text();
			})
			.then((data) => {
				let parser = new DOMParser();
				let doc = parser.parseFromString(data, 'text/html');

				if (
					document.querySelector('.pdp-media-grid') &&
					doc.querySelector('.pdp-media-grid')
				) {
					document.querySelector(
						'.pdp-media-grid'
					).innerHTML = doc.querySelector('.pdp-media-grid').innerHTML;

					pdpMediaCellMagnify();
				}

				if (
					document.querySelector('.pdp-price') &&
					document.querySelector('.pdp-price').querySelector('.price') &&
					doc.querySelector('.pdp-price') &&
					doc.querySelector('.pdp-price').querySelector('.price')
				) {
					document
						.querySelector('.pdp-price')
						.querySelector('.price').innerHTML = doc
						.querySelector('.pdp-price')
						.querySelector('.price').innerHTML;

					if (
						doc
							.querySelector('.pdp-price')
							.querySelector('.price')
							.classList.contains('price--on-sale')
					) {
						document
							.querySelector('.pdp-price')
							.querySelector('.price')
							.classList.add('price--on-sale');
					} else {
						document
							.querySelector('.pdp-price')
							.querySelector('.price')
							.classList.remove('price--on-sale');
					}

					document.querySelector('.pdp-price').style.visibility = 'visible';
				}

				if (
					document.querySelector('.afterpay-instalments') &&
					doc.querySelector('.price-item--regular')
				) {
					let price = parseInt(
						doc
							.querySelector('.price-item--regular')
							.innerHTML.replace(/^\D+/g, '')
					);

					document.querySelector('.afterpay-instalments').innerHTML = `$${(
						price / 4
					).toFixed(2)}`;
				}

				if (doc.querySelector('.product-form__input.option-size')) {
					document.querySelector('#AddToCart').textContent = 'Select Size';
					document.querySelector('#AddToCart').disabled = true;
				} else {
					document.querySelector('#AddToCart').textContent = 'Add to cart';
					document.querySelector('#AddToCart').disabled = false;

					const newForms = document.querySelectorAll(
						`[id^="product-form-template--"], #product-form-installment`
					);
					const variantId = doc
						.querySelector('#product-form-installment')
						?.querySelector('input[name="id"]').value;

					newForms.forEach((productForm) => {
						const input = productForm.querySelector('input[name="id"]');
						input.value = variantId;
						input.dispatchEvent(new Event('change', { bubbles: true }));
					});
				}

				if (
					document.querySelector('.satc_action') &&
					document.querySelector('.satc_action').querySelector('.h6') &&
					doc.querySelector('.satc_action') &&
					doc.querySelector('.satc_action').querySelector('.h6')
				) {
					document
						.querySelector('.satc_action')
						.querySelector('.h6').innerHTML = doc
						.querySelector('.satc_action')
						.querySelector('.h6').innerHTML;
				}

				if (
					document.querySelector('.product-form__input.option-width') &&
					doc.querySelector('.product-form__input.option-width')
				) {
					document.querySelector(
						'.product-form__input.option-width'
					).innerHTML = doc.querySelector(
						'.product-form__input.option-width'
					).innerHTML;

					if (
						selectedWidth &&
						doc
							.querySelector('.product-form__input.option-width')
							.querySelector(`input[value="${selectedWidth.value}"]`)
					) {
						document
							.querySelector('.option-width')
							.querySelector(`label[value="${selectedWidth.value}"]`)
							.click();
					} else {
						document
							.querySelector('.product-form__input.option-width')
							.querySelector(`input:first-child`).checked = true;
					}
				}

				if (
					document.querySelector('.product-form__input.option-size') &&
					doc.querySelector('.product-form__input.option-size')
				) {
					document.querySelector(
						'.product-form__input.option-size'
					).innerHTML = doc.querySelector(
						'.product-form__input.option-size'
					).innerHTML;
				}

				if (
					document.querySelector('.product-variants-json') &&
					doc.querySelector('.product-variants-json')
				) {
					document.querySelector(
						'.product-variants-json'
					).innerHTML = doc.querySelector('.product-variants-json').innerHTML;
				}

				pdpVariantSizeStatus();

				if (
					document.querySelector('.product-value-proposition') &&
					doc.querySelector('.product-value-proposition')
				) {
					document.querySelector(
						'.product-value-proposition'
					).innerHTML = doc.querySelector(
						'.product-value-proposition'
					).innerHTML;
				}

				if (
					document.querySelector('.technology-blocks') &&
					doc.querySelector('.technology-blocks')
				) {
					document.querySelector(
						'.technology-blocks'
					).innerHTML = doc.querySelector('.technology-blocks').innerHTML;
				}

				// klaviyo back in stock
				if (document.querySelector('.klaviyo-bis-trigger')) {
					document.querySelector('.klaviyo-bis-trigger').remove();
				}

				if (document.querySelector('#klaviyo-bis-iframe')) {
					document.querySelector('#klaviyo-bis-iframe').remove();
				}

				if (
					document.querySelector(
						'[src^="https://a.klaviyo.com/media/js/onsite/onsite.js"]'
					)
				) {
					document
						.querySelector(
							'[src^="https://a.klaviyo.com/media/js/onsite/onsite.js"]'
						)
						.remove();
				}

				if (document.querySelector('#scripts-klaviyo-bis')) {
					setTimeout(() => {
						let scriptKlaviyoBis = document.querySelector(
							'#scripts-klaviyo-bis'
						);
						let scriptKlavitoOnSite = document.createElement('script');
						scriptKlavitoOnSite.src = `https://a.klaviyo.com/media/js/onsite/onsite.js`;

						scriptKlaviyoBis.parentNode.insertBefore(
							scriptKlavitoOnSite,
							scriptKlaviyoBis
						);

						scriptKlaviyoBis.innerHTML = doc.querySelector(
							'#scripts-klaviyo-bis'
						).innerHTML;
					}, 200);
				}

				// tracking - GA
				ga('send', 'pageview', location.pathname);

				// tracking - Klaviyo
				document.querySelector(
					'#scripts-klaviyo-viewed-products'
				).innerHTML = doc.querySelector(
					'#scripts-klaviyo-viewed-products'
				).innerHTML;

				// remove transition state
				root.classList.remove('pdp-is-transitioning');
			});

		// update url
		history.pushState(null, null, target.href);
	});

	// sizing popup
	const main = document.querySelector('#MainContent');
	const sizingPopup = document.querySelectorAll('.product-sizing-popup');

	if (sizingPopup) {
		sizingPopup.forEach((el) => {
			const sizingPopupClone = el.cloneNode(true);
			el.remove();
			main.parentNode.insertBefore(sizingPopupClone, main.nextSibling);

			window.addEventListener('click', (e) => {
				if (
					e.target.classList.contains('js-size-chart-open') ||
					e.target.closest('.js-size-chart-open')
				) {
					sizingPopupClone.classList.add('is-active');
				}

				if (
					e.target.classList.contains('js-size-chart-close') ||
					e.target.closest('.js-size-chart-close')
				) {
					sizingPopupClone.classList.remove('is-active');
				}
			});
		});
	} else if (document.querySelector('.js-size-chart-open')) {
		document.querySelector('.js-size-chart-open').remove();
	}

	// media grid
	const mediaGrid = document.querySelector('.pdp-media-grid');
	const mobileScrollBarThumb = document.querySelector('.scrollbar-thumb');

	mediaGrid.addEventListener('scroll', () => {
		let scrollPercentage = parseInt(
			((100 * mediaGrid.scrollLeft) /
				(mediaGrid.scrollWidth - mediaGrid.clientWidth)) *
				(2 / 3)
		);

		mobileScrollBarThumb.style.left = `${scrollPercentage}%`;
	});

	document.querySelectorAll('.product-hero-scroll').forEach((el) => {
		el.addEventListener('click', () => {
			scroll({
				top: document.querySelector('.product-section').clientHeight,
				behavior: 'smooth',
			});
		});
	});
};

const initContentLayout = () => {
	// iframe fit vids
	reframe(
		`
		.plp-interstitial.plp-video-card iframe,
		.content-layout iframe[src*="vimeo.com"], 
		.content-layout iframe[src*="youtube.com"]
		`,
		'video-reframe'
	);

	// unwrap image from paragraph tag
	let contentLayout = document.querySelector('.content-layout');
	if (contentLayout) {
		[].forEach.call(contentLayout.getElementsByTagName('img'), function(img) {
			let p = img.closest('p');

			if (p && p.parentElement) {
				p.parentElement.insertBefore(img, p);
				p.parentElement.removeChild(p);
			}
		});
	}
};

const initCompVideoFull = () => {
	reframe(
		`.comp-video-full iframe[src*="vimeo.com"], 
		.comp-video-full iframe[src*="youtube.com"]`,
		'video-reframe'
	);

	document.querySelectorAll('.comp-video-full').forEach((el) => {
		let iframe = el.querySelector('iframe');
		let player = new Vimeo.Player(iframe);

		el.querySelector('.video-play').addEventListener('click', () => {
			el.classList.add('is-playing');
			player.play();
		});
	});
};

const initCompProductCard = () => {
	document.querySelectorAll('.product-card').forEach((card) => {
		if (card.classList.contains('is-init')) {
			return false;
		} else {
			card.classList.add('is-init');
		}

		let variantData = card.getAttribute('data-product-variants')
			? JSON.parse(
					card
						.getAttribute('data-product-variants')
						.replace(/'/g, '"')
						.trim()
			  )
			: '';

		let quickadd = card.querySelector('.pc-quick-add');
		if (quickadd) {
			let selectors = quickadd.querySelectorAll('.selector');
			let quickAddTrigger = card.querySelector('.pc-quick-add-trigger');

			quickAddTrigger.addEventListener('click', addOn);

			quickAddTrigger.addEventListener('click', () => {
				setTimeout(openBag, 800);
				card.querySelector('.wk-button--remove')
					? card.querySelector('.wk-button--remove').click()
					: '';
			});

			const buttonStates = () => {
				let selectedValues = card.querySelectorAll(
					'.selector-value.is-selected'
				);
				let selectedVariantArray = [];

				selectedValues.forEach((value) => {
					selectedVariantArray.push(value.getAttribute('data-value'));
				});

				if (selectors.length == selectedVariantArray.length) {
					quickadd.classList.add('has-selected-value');

					selectors.forEach((selectors2) => {
						selectors2.classList.remove('is-active');
					});

					variantData.forEach((data) => {
						if (data.title.includes(selectedVariantArray.join(' / '))) {
							if (data.availability) {
								quickAddTrigger.setAttribute('variant', data.id);
								quickAddTrigger.innerHTML = 'Add to Cart';
								quickAddTrigger.disabled = false;
							} else {
								quickAddTrigger.innerHTML = 'Sold Out';
								quickAddTrigger.disabled = true;
							}
						}
					});
				}

				selectors.forEach((selector2, i) => {
					if (i != 1) {
						return false;
					}

					selector2.querySelectorAll('.selector-value').forEach((button) => {
						if (selector2.getAttribute('data-index') == 3) {
							button.parentNode.classList.add('is-not-available');
						}

						variantData.forEach((data) => {
							if (
								data.title.includes(selectedVariantArray.join(' / ')) &&
								data.title
									.split(' / ')
									.includes(button.getAttribute('data-value'))
							) {
								data.availability == false
									? button.classList.add('is-sold')
									: button.classList.remove('is-sold');
							}

							if (
								(data.title.split(' / ')[1] == selectedVariantArray[0] &&
									data.title.split(' / ')[2] ==
										button.getAttribute('data-value')) ||
								selectedVariantArray.length == 1
							) {
								button.parentNode.classList.remove('is-not-available');
							}

							setTimeout(() => {
								if (
									selector2.getAttribute('data-index') == 3 &&
									button.classList.contains('is-selected') &&
									button.parentNode.classList.contains('is-not-available')
								) {
									button.classList.remove('is-selected');
									selector2.querySelector('.selector-label-name').innerHTML =
										'Size';
									quickadd.classList.remove('has-selected-value');
								}
							}, 10);
						});
					});
				});
			};

			selectors.forEach((selector) => {
				selector
					.querySelector('.selector-label')
					.addEventListener('click', () => {
						selectors.forEach((selector2) => {
							if (
								selector2 == selector &&
								!selector.classList.contains('is-active')
							) {
								selector.classList.add('is-active');
							} else {
								selector2.classList.remove('is-active');
							}
						});
					});

				selector.addEventListener('mouseleave', () => {
					selector.classList.remove('is-active');
				});

				selector.querySelectorAll('[data-value]').forEach((button) => {
					button.addEventListener('click', () => {
						selector.querySelector('.selector-label-name').innerHTML =
							button.textContent;

						button.classList.add('is-selected');

						getSiblings(button.closest('li')).forEach((el) => {
							el.querySelector('button').classList.remove('is-selected');
							el.classList.remove('is-not-available');
						});

						buttonStates();
					});
				});

				// select medium width by default
				if (
					selector.dataset.index == '2' &&
					selector.nextElementSibling != null
				) {
					selector.querySelector('[data-value]').click();
				}
			});
		}
	});
};

const initCxPage = () => {
	if (
		window.innerWidth <= 900 &&
		document.querySelector('.cx-title') &&
		document.querySelector('.cx-sidebar')
	) {
		const cxTitle = document.querySelector('.cx-title');
		const cxSidebar = document.querySelector('.cx-sidebar');
		const sidebarClone = cxSidebar.cloneNode(true);

		cxSidebar.remove();
		cxTitle.parentNode.insertBefore(sidebarClone, cxTitle.nextSibling);
	}

	if (document.querySelector('.cx-sidebar')) {
		document.querySelector('.cx-sidebar').style.visibility = 'visible';
	}
};

const initWishlist = () => {
	document.addEventListener('click', (e) => {
		let target = e.target;

		if (
			!target.classList.contains('wk-sharing__link') &&
			!target.closest('.wk-sharing__link') &&
			document.querySelector('.wk-sharing__link') &&
			!document
				.querySelector('.wk-sharing__link')
				.classList.contains('wk-sharing__link--hidden')
		) {
			document
				.querySelector('.wk-sharing__link')
				.classList.add('wk-sharing__link--hidden');
		}
	});
};

// initiate site
window.addEventListener('DOMContentLoaded', (event) => {
	initSearchPane();
	initFlickitySlider();
	initContentLayout();
	initAccordion();
	initCompVideoFull();
	initWishlist();
	initPageAnimation();

	if (!root.id.includes('page-wishlist')) {
		initCompProductCard();
	}

	document.querySelectorAll('.banner-scroll').forEach((el) => {
		el.addEventListener('click', () => {
			scroll({
				top: window.innerHeight,
				behavior: 'smooth',
			});
		});
	});

	document.querySelectorAll('.sticky-add-to-cart-scroll').forEach((el) => {
		el.addEventListener('click', () => {
			console.log(el, 'click!');

			scroll({
				top: window.innerWidth * 1.5,
				behavior: 'smooth',
			});

			document.querySelector('#AddToCart').click();
		});
	});

	// execute page specific functions
	switch (root.id) {
		case 'search-index':
			initSearchIndex();
			break;

		case 'blog-index':
			initBlogIndex();
			break;

		case 'product-single':
			initProductSingle();
			break;

		case 'page-contact':
			initCxPage();
			break;

		case 'page-faq':
			initCxPage();
			break;

		case 'page-payment-shipping':
			initCxPage();
			break;

		case 'page-returns-exchanges':
			initCxPage();
			break;

		case 'page-resoling':
			initCxPage();
			break;
	}

	let rellax = new Rellax('.rellax');

	// make visible .avoid-style-flash elements
	setTimeout(function() {
		document.querySelectorAll('.avoid-style-flash').forEach((el) => {
			el.style.visibility = 'visible';
		});
	}, 400);
});
