let header = document.getElementById('shopify-section-header');
let menuTrigger = document.querySelectorAll('.header-menu_item a');

header.addEventListener('mouseleave', closeAllMenus, false);

for (var i = 0; i < menuTrigger.length; i++) {
    menuTrigger[i].addEventListener('mouseover', openMenu, false);
}

function openMenu() {
    let menu = this.getAttribute('show-menu');
    let otherMenus = document.querySelectorAll('.menu-block-container');
    for (var i = 0; i < otherMenus.length; i++) {
        otherMenus[i].setAttribute('visible', '');
    }
    document.querySelector('.menu-block-container[type="' + menu + '"]').setAttribute('visible', 'true');
    this.addEventListener('mouseleave', closeThisMenu, false);
}

function closeThisMenu() {
    let menu = this.getAttribute('show-menu');
    setTimeout(function(){
        document.querySelector('.menu-block-container[type="' + menu + '"]').setAttribute('visible', '');
    }, 300);
}

function closeAllMenus() {
    let allMenus = document.querySelectorAll('.menu-block-container');
    for (var i = 0; i < allMenus.length; i++) {
        allMenus[i].setAttribute('visible', '');
    }
}

// Mobile functions

let openMenuAction = document.querySelectorAll('*[action="open-mobile-menu"]');

for (var i = 0; i < openMenuAction.length; i++) {
    openMenuAction[i].addEventListener('click', openMobileMenu, false);
}

function openMobileMenu() {

    let mobileMenu = document.querySelector('.mobile-menu-container');

    if (mobileMenu.getAttribute('visible') === 'false') {
        mobileMenu.setAttribute('visible', 'true');
        document.querySelector('#shopify-section-announcement-bar').setAttribute('visible', 'false');
    } else {
        mobileMenu.setAttribute('visible', 'false');
        document.querySelector('#shopify-section-announcement-bar').setAttribute('visible', 'true');
    }
    
}

let categoryTrigger = document.querySelectorAll('.mobile-menu-container .select-category');

for (var i = 0; i < categoryTrigger.length; i++) {
    categoryTrigger[i].addEventListener('click', switchCategory, false);
}

function switchCategory() {

    let selection = this.getAttribute('show-menu');

    this.setAttribute('state', 'active');
    document.querySelector('.mobile-menu-container .select-category:not([show-menu="' + selection + '"])').setAttribute('state', 'inactive');

    let menus = document.querySelectorAll('.category-by-type-container');

    for (var i = 0; i < menus.length; i++) {
        menus[i].setAttribute('state', 'inactive');
    }

    let selectedMenus = document.querySelectorAll('.category-by-type-container[category="' + selection + '"]');

    for (var i = 0; i < selectedMenus.length; i++) {
        selectedMenus[i].setAttribute('state', 'active');
    }

}