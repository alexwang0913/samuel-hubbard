class LoginRegister extends HTMLElement {
	constructor() {
		super();

		const resetPasswordButton = this.querySelector(
			'.login-page__forgot-password-button'
		);
		resetPasswordButton.addEventListener(
			'click',
			this.showResetPassworForm.bind(this)
		);

		if (location.hash == '#recover' || redirectToRecover) {
			resetPasswordButton.click();
		}

		setTimeout(() => {
			if (redirectToRecover) {
				resetPasswordButton.click();
			}
		}, 800);

		this.querySelector('.login-page__cancel-reset-button').addEventListener(
			'click',
			this.hideResetPasswordForm.bind(this)
		);
	}

	showResetPassworForm() {
		history.pushState(null, null, '#recover');
		this.classList.add('reset-password');
	}

	hideResetPasswordForm() {
		this.classList.remove('reset-password');
	}
}

customElements.define('login-register', LoginRegister);

class PasswordInput extends HTMLElement {
	constructor() {
		super();

		this.querySelector('button').addEventListener(
			'click',
			this.togglePasswordVisibility.bind(this)
		);
	}

	togglePasswordVisibility(event) {
		event.preventDefault();

		const passwordInput = this.querySelector('input');
		const toggleButton = this.querySelector('button');
		if (passwordInput.type == 'password') {
			passwordInput.type = 'text';
			toggleButton.classList.add('show');
		} else {
			passwordInput.type = 'password';
			toggleButton.classList.remove('show');
		}
	}
}

customElements.define('password-input', PasswordInput);
