const bag = document.querySelector('.bag-container');
let bagAddOnCartProducts = bag.querySelectorAll('.add-cart-item');
let bagAddOnProductsVariantSelector = bag.querySelectorAll(
	'.item-variant-selector'
);

for (var i = 0; i < bagAddOnProductsVariantSelector.length; i++) {
	bagAddOnProductsVariantSelector[i].addEventListener(
		'change',
		bagUpdateVariant,
		false
	);
}

function bagUpdateVariant() {
	let item = this.closest('.line_item');
	let variantsJson = JSON.parse(item.querySelector('script').textContent);
	let selectedVariants = [];
	let variantSelectors = item.querySelectorAll('.item-variant-selector');

	for (let i = 0; i < variantSelectors.length; i++) {
		selectedVariants.push(variantSelectors[i].value);
	}

	for (let i = 0; i < variantsJson.length; i++) {
		if (variantsJson[i].title == selectedVariants.join(' / ')) {
			item
				.querySelector('.add-item')
				.setAttribute('variant', variantsJson[i].id);
		}
	}
}

for (var i = 0; i < bagAddOnCartProducts.length; i++) {
	bagAddOnCartProducts[i].addEventListener('click', addOn, false);
}

function addOn() {
	let variant = this.getAttribute('variant');

	let formData = {
		items: [
			{
				id: variant,
				quantity: 1,
			},
		],
	};

	fetch('/cart/add.js', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(formData),
	})
		.then((response) => {
			if (response.ok) {
				updateBag();
				return response.json();
			}
		})
		.catch((error) => {
			console.warn('Something went wrong.', error);
		});
}

function removecartItemFromAddOn() {
	let cartItems = bag
		.querySelector('.bag-contents')
		.querySelectorAll('.line_item');
	let cartItemsId = [];
	let bagAccessories = bag.querySelector('.bag-accessories');
	let addOnItems = bagAccessories.querySelectorAll('.line_item');

	for (let i = 0; i < cartItems.length; i++) {
		cartItemsId.push(cartItems[i].dataset.productId);
	}

	for (let i = 0; i < addOnItems.length; i++) {
		if (cartItemsId.includes(addOnItems[i].dataset.productId)) {
			addOnItems[i].classList.add('is-hidden');
		} else {
			addOnItems[i].classList.remove('is-hidden');
		}
	}

	let addOnItemsNotHidden = bagAccessories.querySelectorAll(
		'.line_item:not(.is-hidden)'
	);

	if (addOnItemsNotHidden.length == 0) {
		bagAccessories.classList.add('is-hidden');
	} else {
		bagAccessories.classList.remove('is-hidden');
	}
}
removecartItemFromAddOn();

function updateBag() {
	let container = document.querySelector('.bag-contents');
	let subtotal = document.querySelectorAll('.cart-subtotal-data');
	let unlessEmpty = document.querySelectorAll('.visible-unless-empty');

	const cartContents = fetch('/cart.js')
		.then((response) => response.json())
		.then((data) => {
			let items = data.items;

			if (data.item_count === 0) {
				for (var i = 0; i < unlessEmpty.length; i++) {
					unlessEmpty[i].setAttribute('bag-state', 'empty');
				}
				container.innerHTML =
					'<div class="flex_justify-center mini-bag-empty"><h6 class="h6">Your bag is empty.</h6></div>';
			} else {
				for (var i = 0; i < unlessEmpty.length; i++) {
					unlessEmpty[i].setAttribute('bag-state', 'packed');
				}
				container.innerHTML =
					'<div class="bag-headers small-hide"> <div class="bag-header body-medium">Product</div> <div class="bag-header body-medium">Price</div> <div class="bag-header body-medium">Quantity</div> <div class="bag-header body-medium">Total</div> </div>';
			}

			for (var each in items) {
				let item = items[each];
				let id = item.id;
				let productId = item.product_id;
				let image = item.image;
				let title = item.product_title;
				let color = item.variant_options[0] ? item.variant_options[0] : '';
				let width = item.variant_options[1] ? item.variant_options[1] : '';
				let size = item.variant_options[2] ? item.variant_options[2] : '';
				let quantity = item.quantity;
				let price = (item.final_price / 100).toLocaleString('en-US', {
					style: 'currency',
					currency: 'USD',
					minimumFractionDigits: 0,
					maximumFractionDigits: 0,
				});
				let line_price = (item.final_line_price / 100).toLocaleString('en-US', {
					style: 'currency',
					currency: 'USD',
					minimumFractionDigits: 0,
					maximumFractionDigits: 0,
				});

				let line_item =
					'<article class="line_item" data-product-id="' +
					productId +
					'"><div class="item-image"><span class="object-contain bg-subtle"><img src="' +
					image +
					'"/></span></div> <div class="item_details"> <h6 class="h6">' +
					title +
					'</h6> <p class="utility-thin">' +
					color +
					'</p> <p class="utility-thin">' +
					width +
					' ' +
					size +
					'</p> </div> <div class="item_price small-hide"><p class="body-tall">' +
					price +
					'</p></div> <div class="quantity-container"><div class="adjust-quantity" action="decrement" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="body-small">&#8722;</p></div><div class="quantity-amount body-small">' +
					quantity +
					'</div><div class="adjust-quantity" action="increment" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="body-small">&#xFF0B;</p></div> <div class="remove-item adjust-quantity" action="remove" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="utility">Remove</p></div> </div> <div class="item_line_price small-hide"><p class="body-tall">' +
					line_price +
					'</p></div> </article>';
				container.innerHTML += line_item;

				let quantityAdjusters = document.querySelectorAll('.adjust-quantity');

				for (var i = 0; i < quantityAdjusters.length; i++) {
					quantityAdjusters[i].addEventListener('click', adjustQuantity, false);
				}
			}

			removecartItemFromAddOn();

			let subtotalAmount = (data.items_subtotal_price / 100).toLocaleString(
				'en-US',
				{
					style: 'currency',
					currency: 'USD',
				}
			);

			for (var i = 0; i < subtotal.length; i++) {
				subtotal[i].innerHTML = subtotalAmount;
			}
		});
}

function adjustQuantity() {
	let action = this.getAttribute('action');
	let line = this.getAttribute('line');
	let quantity = this.getAttribute('quantity');

	switch (action) {
		case 'increment':
			quantity++;
			break;
		case 'decrement':
			quantity--;
			break;
		case 'remove':
			quantity = 0;
	}

	let adjustment = {};
	adjustment[line] = quantity;

	let update = { updates: adjustment };

	fetch('/cart/update.js', {
		method: 'POST',
		body: JSON.stringify(update),
		headers: {
			'Content-type': 'application/json; charset=UTF-8',
		},
	})
		.then(function(response) {
			if (response.ok) {
				updateBag();
				return response.json();
			}
			return Promise.reject(response);
		})
		.then(function(data) {})
		.catch(function(error) {
			console.warn('Something went wrong.', error);
		});
}

updateBag();

// class CartRemoveButton extends HTMLElement {
//   constructor() {
//     super();
//     this.addEventListener('click', (event) => {
//       event.preventDefault();
//       this.closest('cart-items').updateQuantity(this.dataset.index, 0);
//     });
//   }
// }

// customElements.define('cart-remove-button', CartRemoveButton);

// class CartItems extends HTMLElement {
//   constructor() {
//     super();

//     this.lineItemStatusElement = document.getElementById('shopping-cart-line-item-status');

//     this.currentItemCount = Array.from(this.querySelectorAll('[name="updates[]"]'))
//       .reduce((total, quantityInput) => total + parseInt(quantityInput.value), 0);

//     this.debouncedOnChange = debounce((event) => {
//       this.onChange(event);
//     }, 300);

//     this.addEventListener('change', this.debouncedOnChange.bind(this));
//   }

//   onChange(event) {
//     this.updateQuantity(event.target.dataset.index, event.target.value, document.activeElement.getAttribute('name'));
//   }

//   getSectionsToRender() {
//     return [
//       {
//         id: 'main-cart-items',
//         section: document.getElementById('main-cart-items').dataset.id,
//         selector: '.js-contents',
//       },
//       {
//         id: 'cart-icon-bubble',
//         section: 'cart-icon-bubble',
//         selector: '.shopify-section'
//       },
//       {
//         id: 'cart-live-region-text',
//         section: 'cart-live-region-text',
//         selector: '.shopify-section'
//       },
//       {
//         id: 'main-cart-footer',
//         section: document.getElementById('main-cart-footer').dataset.id,
//         selector: '.js-contents',
//       }
//     ];
//   }

//   updateQuantity(line, quantity, name) {
//     this.enableLoading(line);

//     const body = JSON.stringify({
//       line,
//       quantity,
//       sections: this.getSectionsToRender().map((section) => section.section),
//       sections_url: window.location.pathname
//     });

//     fetch(`${routes.cart_change_url}`, {...fetchConfig(), ...{ body }})
//       .then((response) => {
//         return response.text();
//       })
//       .then((state) => {
//         const parsedState = JSON.parse(state);
//         this.classList.toggle('is-empty', parsedState.item_count === 0);
//         document.getElementById('main-cart-footer')?.classList.toggle('is-empty', parsedState.item_count === 0);

//         this.getSectionsToRender().forEach((section => {
//           const elementToReplace =
//             document.getElementById(section.id).querySelector(section.selector) || document.getElementById(section.id);

//           elementToReplace.innerHTML =
//             this.getSectionInnerHTML(parsedState.sections[section.section], section.selector);
//         }));

//         this.updateLiveRegions(line, parsedState.item_count);
//         document.getElementById(`CartItem-${line}`)?.querySelector(`[name="${name}"]`)?.focus();
//         this.disableLoading();
//       }).catch(() => {
//         this.querySelectorAll('.loading-overlay').forEach((overlay) => overlay.classList.add('hidden'));
//         document.getElementById('cart-errors').textContent = window.cartStrings.error;
//         this.disableLoading();
//       });
//   }

//   updateLiveRegions(line, itemCount) {
//     if (this.currentItemCount === itemCount) {
//       document.getElementById(`Line-item-error-${line}`)
//         .querySelector('.cart-item__error-text')
//         .innerHTML = window.cartStrings.quantityError.replace(
//           '[quantity]',
//           document.getElementById(`Quantity-${line}`).value
//         );
//     }

//     this.currentItemCount = itemCount;
//     this.lineItemStatusElement.setAttribute('aria-hidden', true);

//     const cartStatus = document.getElementById('cart-live-region-text');
//     cartStatus.setAttribute('aria-hidden', false);

//     setTimeout(() => {
//       cartStatus.setAttribute('aria-hidden', true);
//     }, 1000);
//   }

//   getSectionInnerHTML(html, selector) {
//     return new DOMParser()
//       .parseFromString(html, 'text/html')
//       .querySelector(selector).innerHTML;
//   }

//   enableLoading(line) {
//     document.getElementById('main-cart-items').classList.add('cart__items--disabled');
//     this.querySelectorAll('.loading-overlay')[line - 1].classList.remove('hidden');
//     document.activeElement.blur();
//     this.lineItemStatusElement.setAttribute('aria-hidden', false);
//   }

//   disableLoading() {
//     document.getElementById('main-cart-items').classList.remove('cart__items--disabled');
//   }
// }

// customElements.define('cart-items', CartItems);
