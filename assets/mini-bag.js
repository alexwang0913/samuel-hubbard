let openBagAction = document.querySelectorAll('*[action="open-mini-bag"]');

for (var i = 0; i < openBagAction.length; i++) {
	openBagAction[i].addEventListener('click', openBag, false);
}

function openBag() {
	document.querySelector('mini-bag').setAttribute('visible', 'true');
	document.querySelector('#overlay-dark').setAttribute('state', 'active');
}

document.querySelectorAll('.close-mini-bag').forEach((el) => {
	el.addEventListener('click', function() {
		document.querySelector('mini-bag').setAttribute('visible', 'false');
		document.querySelector('#overlay-dark').setAttribute('state', 'inactive');
	});
});

const miniBag = document.querySelector('mini-bag');
let addOnProducts = miniBag.querySelectorAll('.add-item');
let addOnProductsVariantSelector = miniBag.querySelectorAll(
	'.item-variant-selector'
);

for (var i = 0; i < addOnProductsVariantSelector.length; i++) {
	addOnProductsVariantSelector[i].addEventListener(
		'change',
		updateVariant,
		false
	);
}

function updateVariant() {
	let item = this.closest('.line_item');
	let variantsJson = JSON.parse(item.querySelector('script').textContent);
	let selectedVariants = [];
	let variantSelectors = item.querySelectorAll('.item-variant-selector');

	for (let i = 0; i < variantSelectors.length; i++) {
		selectedVariants.push(variantSelectors[i].value);
	}

	for (let i = 0; i < variantsJson.length; i++) {
		if (variantsJson[i].title == selectedVariants.join(' / ')) {
			item
				.querySelector('.add-item')
				.setAttribute('variant', variantsJson[i].id);
		}
	}
}

for (var i = 0; i < addOnProducts.length; i++) {
	addOnProducts[i].addEventListener('click', addOn, false);
}

function addOn() {
	let variant = this.getAttribute('variant');

	let formData = {
		items: [
			{
				id: variant,
				quantity: 1,
			},
		],
	};

	fetch('/cart/add.js', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(formData),
	})
		.then((response) => {
			if (response.ok) {
				updateMiniBag();
				return response.json();
			}
		})
		.catch((error) => {
			alert('Item is out of stock');
			console.warn('Something went wrong.', error);
		});
}

function removecartItemFromAddOn() {
	let cartItems = miniBag
		.querySelector('.mini-bag-contents')
		.querySelectorAll('.line_item');
	let cartItemsId = [];
	let miniBagAccessories = miniBag.querySelector('.mini-bag-accessories');
	let addOnItems = miniBagAccessories.querySelectorAll('.line_item');

	for (let i = 0; i < cartItems.length; i++) {
		cartItemsId.push(cartItems[i].dataset.productId);
	}

	for (let i = 0; i < addOnItems.length; i++) {
		if (cartItemsId.includes(addOnItems[i].dataset.productId)) {
			addOnItems[i].classList.add('is-hidden');
		} else {
			addOnItems[i].classList.remove('is-hidden');
		}
	}

	let addOnItemsNotHidden = miniBagAccessories.querySelectorAll(
		'.line_item:not(.is-hidden)'
	);

	if (addOnItemsNotHidden.length == 0) {
		miniBagAccessories.classList.add('is-hidden');
	} else {
		miniBagAccessories.classList.remove('is-hidden');
	}
}
removecartItemFromAddOn();

function updateMiniBag() {
	let container = document.querySelector('.mini-bag-contents');
	let subtotal = document.querySelector('#mini-bag-subtotal');
	let unlessEmpty = document.querySelectorAll('.visible-unless-empty');

	const cartContents = fetch('/cart.js')
		.then((response) => response.json())
		.then((data) => {
			let items = data.items;

			document.querySelectorAll('.cart-item-count').forEach((el) => {
				el.innerText = data.item_count;
			});

			if (data.item_count === 0) {
				for (var i = 0; i < unlessEmpty.length; i++) {
					unlessEmpty[i].setAttribute('bag-state', 'empty');
				}
				container.innerHTML = `<div class="mini-bag-empty type-spacing">
						<h6 class="h6">Your bag is empty.</h6>
						<button class="button button--primary cr-black close-mini-bag">Continue Shopping</button>
					</div>`;

				document.querySelectorAll('.close-mini-bag').forEach((el) => {
					el.addEventListener('click', function() {
						document.querySelector('mini-bag').setAttribute('visible', 'false');
						document
							.querySelector('#overlay-dark')
							.setAttribute('state', 'inactive');
					});
				});
			} else {
				for (var i = 0; i < unlessEmpty.length; i++) {
					unlessEmpty[i].setAttribute('bag-state', 'packed');
				}
				container.innerHTML = '';
			}

			for (var each in items) {
				let item = items[each];
				let id = item.id;
				let productId = item.product_id;
				let image = item.image;
				let title = item.product_title;
				let color = item.variant_options[0] ? item.variant_options[0] : '';
				let width = item.variant_options[1] ? item.variant_options[1] : '';
				let size = item.variant_options[2] ? item.variant_options[2] : '';
				let quantity = item.quantity;
				let price = (item.original_line_price / 100).toLocaleString('en-US', {
					style: 'currency',
					currency: 'USD',
					minimumFractionDigits: 0,
					maximumFractionDigits: 0,
				});

				let line_item =
					'<article class="line_item" data-product-id="' +
					productId +
					'"><div class="item-image"><span class="object-contain bg-subtle"><img src="' +
					image +
					'"/> </span></div><div class="item_details"> <h6 class="h6">' +
					title +
					'</h6> <p class="utility-thin">' +
					color +
					'</p> <p class="utility-thin">' +
					width +
					' ' +
					size +
					'</p> </div> <div class="item_price"><p class="body-tall">' +
					price +
					'</p></div> <div class="quantity-container"><div class="adjust-quantity" action="decrement" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="body-small">&#8722;</p></div><div class="quantity-amount body-small">' +
					quantity +
					'</div><div class="adjust-quantity" action="increment" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="body-small">&#xFF0B;</p></div></div> <div class="remove-item adjust-quantity" action="remove" line="' +
					id +
					'" quantity="' +
					quantity +
					'"><p class="body-small">&#x2715</p></div> </article>';
				container.innerHTML += line_item;

				let quantityAdjusters = document.querySelectorAll('.adjust-quantity');

				for (var i = 0; i < quantityAdjusters.length; i++) {
					quantityAdjusters[i].addEventListener('click', adjustQuantity, false);
				}
			}

			removecartItemFromAddOn();

			let subtotalAmount = (data.items_subtotal_price / 100).toLocaleString(
				'en-US',
				{
					style: 'currency',
					currency: 'USD',
				}
			);

			subtotal.innerHTML = subtotalAmount;
		});
}

function adjustQuantity() {
	let action = this.getAttribute('action');
	let line = this.getAttribute('line');
	let quantity = this.getAttribute('quantity');

	switch (action) {
		case 'increment':
			quantity++;
			break;
		case 'decrement':
			quantity--;
			break;
		case 'remove':
			quantity = 0;
	}

	let adjustment = {};
	adjustment[line] = quantity;

	let update = { updates: adjustment };

	fetch('/cart/update.js', {
		method: 'POST',
		body: JSON.stringify(update),
		headers: {
			'Content-type': 'application/json; charset=UTF-8',
		},
	})
		.then(function(response) {
			if (response.ok) {
				updateMiniBag();
				return response.json();
			}
			return Promise.reject(response);
		})
		.then(function(data) {})
		.catch(function(error) {
			console.warn('Something went wrong.', error);
		});
}

updateMiniBag();
