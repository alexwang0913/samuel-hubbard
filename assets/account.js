class AccountPage extends HTMLElement {
	constructor() {
		super();

		this.desktopNewAddressButton = this.querySelector(
			'.account__add-new-address-cta--desktop'
		);
		this.mobileNewAddressButton = this.querySelector(
			'.account__add-new-address-cta--mobile'
		);
		this.desktopEditAddressButtons = Array.from(
			this.querySelectorAll('.account__edit-address-button--desktop')
		);
		this.mobileEditAddressButtons = Array.from(
			this.querySelectorAll('.account__edit-address-button--mobile')
		);
		this.accountSelect = this.querySelector('.account__nav-select');

		if (window.location.hash == '#add-address') {
			this.showAddressContent();
		}

		if (
			window.location.pathname.includes('account') &&
			!window.location.pathname.includes('orders')
		) {
			this.accountSelect.value = window.location.pathname;
		}

		this.initEventCallbacks();
	}

	openAddAddressDialog() {
		const addressDialog = document.querySelector('address-dialog');
		const addAddressElement = document.getElementById('AddAddress');
		addressDialog.open(addAddressElement.innerHTML);

		new Shopify.CountryProvinceSelector(
			'AddressCountryNew',
			'AddressProvinceNew',
			{
				hideElement: 'AddressProvinceContainerNew',
			}
		);
	}

	openEditAddressDialog(event) {
		const addressId = event.target.dataset.addressId;
		const editAddressForm = document.getElementById(`EditAddress_${addressId}`);
		const addressDialog = document.querySelector('address-dialog');
		addressDialog.open(editAddressForm.innerHTML);

		new Shopify.CountryProvinceSelector(
			`AddressCountry_${addressId}`,
			`AddressProvince_${addressId}`,
			{
				hideElement: `AddressProvinceContainer_${addressId}`,
			}
		);
	}

	navigateAccountPage(event) {
		window.location.href = event.target.value;
	}

	showAddressContent() {
		if (window.location.hash != '#add-address') {
			window.location.href += '#add-address';
		}
		const accountContent = this.querySelector('.account__content');
		accountContent.classList.add('hidden');

		const addressContainer = this.querySelector(
			'.account__mobile-address-container'
		);

		const addressContent = addressContainer.querySelector(
			'.account__mobile-address-content'
		);
		addressContent.innerHTML = document.getElementById('AddAddress').innerHTML;

		const backButton = addressContainer.querySelector('.account__back-button');
		backButton.addEventListener('click', this.hideAddressContent.bind(this));

		addressContainer.classList.remove('hidden');
		window.scrollTo(0, 0);
	}

	hideAddressContent() {
		window.location.href = window.location.href.split('#')[0];
	}

	showEditAddressContent(event) {
		const addressId = event.target.dataset.addressId;
		if (!window.location.hash.includes('#edit-address')) {
			window.location.href += `#edit-address?${addressId}`;
		}
		const accountContent = this.querySelector('.account__content');
		accountContent.classList.add('hidden');

		const addressContainer = this.querySelector(
			'.account__mobile-address-container'
		);

		const addressContent = addressContainer.querySelector(
			'.account__mobile-address-content'
		);
		addressContent.innerHTML = document.getElementById(
			`EditAddress_${addressId}`
		).innerHTML;

		const backButton = addressContainer.querySelector('.account__back-button');
		backButton.addEventListener('click', this.hideAddressContent.bind(this));

		addressContainer.classList.remove('hidden');
		addressProvinceInit();
	}

	initEventCallbacks() {
		if (this.desktopNewAddressButton) {
			this.desktopNewAddressButton.addEventListener(
				'click',
				this.openAddAddressDialog.bind(this)
			);
		}

		if (this.mobileNewAddressButton) {
			this.mobileNewAddressButton.addEventListener(
				'click',
				this.showAddressContent.bind(this)
			);
		}

		if (this.desktopEditAddressButtons) {
			this.desktopEditAddressButtons.forEach((editButton) =>
				editButton.addEventListener(
					'click',
					this.openEditAddressDialog.bind(this)
				)
			);
		}

		if (this.mobileEditAddressButtons) {
			this.mobileEditAddressButtons.forEach((editButton) =>
				editButton.addEventListener(
					'click',
					this.showEditAddressContent.bind(this)
				)
			);
		}

		if (this.accountSelect) {
			this.accountSelect.addEventListener(
				'change',
				this.navigateAccountPage.bind(this)
			);
		}
	}
}

customElements.define('account-page', AccountPage);

class AddressDialog extends HTMLElement {
	constructor() {
		super();

		this.addEventListener('click', (event) => {
			if (event.target.nodeName == 'ADDRESS-DIALOG') {
				this.hide();
			}
		});
	}

	open(content) {
		this.innerHTML = content;

		const checkboxContainer = this.querySelector(
			'.set-as-default-checkbox-container'
		);
		if (checkboxContainer) {
			const time = new Date().getTime();
			const defaultCheckbox = checkboxContainer.querySelector('input');
			defaultCheckbox.id = `address_default_${time}`;

			const checkboxLabel = checkboxContainer.querySelector('label');
			checkboxLabel.htmlFor = `address_default_${time}`;
		}

		this.querySelector('.address-dialog__close-button').addEventListener(
			'click',
			this.hide.bind(this)
		);
		this.classList.add('open');
	}

	hide() {
		this.classList.remove('open');
	}
}

customElements.define('address-dialog', AddressDialog);
