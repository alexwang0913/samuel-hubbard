const storeLocatorSections = document.querySelectorAll(
	'.store-locator-section'
);

if (window.location.pathname == '/apps/store-locator') {
	storeLocatorSections.forEach((section) => (section.style.display = 'block'));

	const headingElement = document.createElement('h2');
	headingElement.textContent = 'Result';
	headingElement.classList.add('store-locatore__heading');

	const content = document.getElementById('col-main');
	content.classList.add('store-locatore__content');
	content.appendChild(headingElement);

	const locatorIcon = document.querySelector(
		'.store-locator__locator-icon svg'
	);

	const addressElements = document.querySelectorAll('.addresses li');
	addressElements.forEach((element) => {
		const iconWrapper = document.createElement('div');
		const cloneIcon = locatorIcon.cloneNode(true);
		iconWrapper.appendChild(cloneIcon);

		const directionContainer = document.createElement('div');
		directionContainer.classList.add('store-locatore__direction-container');
		directionContainer.appendChild(cloneIcon);

		const directionLabel = document.createElement('div');
		directionLabel.classList.add('body-medium');
		directionLabel.classList.add('store-locatore__direction-label');
		directionLabel.innerHTML = `<a 
		target="_blank" 
			href="https://www.google.com/maps/place/${
				element.querySelector('.address').innerText
			}+${element.querySelector('.prov_state').innerText}+${
			element.querySelector('.postal_zip').innerText
		}">Get Directions</a>`;

		directionContainer.appendChild(directionLabel);
		element.appendChild(directionContainer);

		element.addEventListener('click', (event) => {
			addressElements.forEach((element) => element.classList.remove('active'));
			element.classList.add('active');
		});
	});
}
