const slide = (action, el, duration = 400) => {
	const slideDown = () => {
		let childrenHeight = Object.values(el.children).reduce(
			(total, i) => total + i.offsetHeight,
			0
		);

		// Apply active state
		el.classList.add('active');
		el.style.height = 'auto';

		// Set the height of the content as 0px
		// so we can trigger the slide down animation.
		el.style.height = '0px';

		// Do this after the 0px has applied.
		// It's like a delay or something. MAGIC!
		setTimeout(() => {
			el.style.height = `${childrenHeight}px`;
			el.style.transition = `height ${duration}ms`;
		}, 0);
	};

	const slideUp = () => {
		let childrenHeight = Object.values(el.children).reduce(
			(total, i) => total + i.offsetHeight,
			0
		);

		// Set the height as 0px to trigger the slide up animation.
		el.classList.remove('active');
		el.style.height = '0px';
		el.style.transition = `height ${duration}ms`;

		// Remove the active state when the animation ends
		el.addEventListener(
			'transitionend',
			() => {
				el.style.transition = `0s`;
			},
			{ once: true }
		);
	};

	switch (action) {
		case 'down':
			slideDown();
			break;

		case 'up':
			slideUp();
			break;

		case 'toggle':
			!el.classList.contains('active') ? slideDown() : slideUp();
			break;
	}
};

let getSiblings = function(e) {
	// for collecting siblings
	let siblings = [];
	// if no parent, return no sibling
	if (!e.parentNode) {
		return siblings;
	}
	// first child of the parent node
	let sibling = e.parentNode.firstChild;

	// collecting siblings
	while (sibling) {
		if (sibling.nodeType === 1 && sibling !== e) {
			siblings.push(sibling);
		}
		sibling = sibling.nextSibling;
	}
	return siblings;
};

const initFlickitySlider = (slider) => {
	const flickitySlider = slider
		? [slider]
		: document.querySelectorAll('[data-flickity-slider]');

	const flickitySliderTrigger = () => {
		flickitySlider.forEach((el) => {
			let flickity = new Flickity(
				el,
				JSON.parse(el.dataset.flickitySlider.replace(/'/g, '"').trim())
			);

			if (slider) {
				flickity.resize();
			}

			let childrenWidth = Object.values(
				el.querySelector('.flickity-slider').childNodes
			).reduce(
				(total, i) =>
					i.offsetWidth
						? total +
						  i.offsetWidth +
						  parseInt(window.getComputedStyle(i).marginLeft) +
						  parseInt(window.getComputedStyle(i).marginRight)
						: total,
				0
			);

			setTimeout(() => {
				el.classList.add('is-initiated');
			}, 1);

			flickity.on('ready', () => {
				el.querySelector('.flickity-slider').setAttribute('role', 'region');
				el.querySelector('.flickity-slider').setAttribute(
					'aria-label',
					'Carousel'
				);
				el.querySelector('.flickity-slider').setAttribute(
					'aria-live',
					'polite'
				);

				el.querySelector('.flickity-prev-next-button.previous')
					? el
							.querySelector('.flickity-prev-next-button.previous')
							.setAttribute('aria-label', 'Previous slide')
					: '';

				el.querySelector('.flickity-prev-next-button.next')
					? el
							.querySelector('.flickity-prev-next-button.next')
							.setAttribute('aria-label', 'Next slide')
					: '';
			});

			if (
				// if total children width is lesser than slider width
				el.getBoundingClientRect().width >= childrenWidth ||
				// if flickity slider has breakpoint set, and window is greater than breakpoint
				(el.dataset.flickityBreakpoint &&
					window.innerWidth > parseInt(el.dataset.flickityBreakpoint))
			) {
				// destroy flickity
				flickity.destroy();

				setTimeout(() => {
					el.querySelectorAll('[style*="left:"]').forEach((item) => {
						item.style.setProperty('left', 'unset');
					});
				}, 400);
			} else if (el.classList.contains('is-draggable')) {
				// avoid click on drag: https://github.com/metafizzy/flickity/issues/838
				flickity.on('dragStart', () => {
					flickity.slider.childNodes.forEach(
						(slide) => (slide.style.pointerEvents = 'none')
					);
				});

				flickity.on('dragEnd', () => {
					flickity.slider.childNodes.forEach(
						(slide) => (slide.style.pointerEvents = 'all')
					);
				});
			}
		});
	};

	flickitySliderTrigger();
	window.addEventListener('resize', flickitySliderTrigger);

	// swipe fix for IOS
	var touchingCarousel = false;
	var touchStartCoords;

	document.body.addEventListener('touchstart', function(e) {
		if (e.target.closest('.flickity-slider')) {
			touchingCarousel = true;
		} else {
			touchingCarousel = false;
			return;
		}

		touchStartCoords = {
			x: e.touches[0].pageX,
			y: e.touches[0].pageY,
		};
	});

	document.body.addEventListener(
		'touchmove',
		function(e) {
			if (!(touchingCarousel && e.cancelable)) {
				return;
			}

			var moveVector = {
				x: e.touches[0].pageX - touchStartCoords.x,
				y: e.touches[0].pageY - touchStartCoords.y,
			};

			if (Math.abs(moveVector.x) > 7) {
				e.preventDefault();
			}
		},
		{
			passive: false,
		}
	);
};

// event delegation
// example; on('body', 'click', '.accordion-toggle, .accordion-toggle *', e => {…});
const on = (selector, eventType, childSelectors, eventHandler) => {
	childSelectors = childSelectors.split(',');
	let elements = document.querySelectorAll(selector);
	for (element of elements) {
		element.addEventListener(eventType, (eventOnElement) => {
			childSelectors.forEach((selector) => {
				if (
					eventOnElement.target.matches(selector) ||
					eventOnElement.target.closest(selector)
				) {
					eventHandler(eventOnElement);
				}
			});
		});
	}
};
